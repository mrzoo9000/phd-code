/*********************************************************************/
/*                                                                   */
/*  FILENAME : ~/CExamples/GAs_1/brandomHill.c                       */
/*                                                                   */
/*  AUTHOR : K Williams                                              */
/*                                                                   */
/*  LAST UPDATE : 9 / May / 1998                                     */
/*                                                                   */
/*  COMMENTS :                                                       */
/*                                                                   */
/*  This is a (purely random) iterative hill-climbing algorithm      */
/*                                                                   */
/*  COMPILER : gcc -O -o brandomHill brandomHill.c -lm               */
/*                                                                   */
/*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Change any of these parameters to match your needs */

#define NVARS 10                 /* no. of problem variables */
#define POPSIZE  1               /* population size */
#define PMUTATION 0.2            /* Probability of muutation */
#define MAXGENS 500              /* max. number of generations */
#define TRUE 1
#define FALSE 0

int generation;                  /* current generation no. */
         /* in Hill-Climbing : generation = noOfHills to climb */

FILE *galog;                     /* an output file */

struct genotype  /* genotype (GT), a member of the population */
{
	int gene[NVARS];      /* a string of variables */
	int fitness;          /* GT`s fitness */
	double rfitness;         /* relative fitness */
	double cfitness;         /* cumulative fitness */
};

struct genotype population[POPSIZE+1];        /* population */

/* Prototypes of functions used by this genetic algorithm */

void initialize(void);
int randbit();
void evaluate(void);
void report(void);


void printPop()
{
  int i, j;

  printf("\nGENERATION %d\n", generation);

  for (i=0; i<=POPSIZE; i++) {
    printf("\n*** population[%d] Fitness = %d\n", i, population[i].fitness);
    for (j=0; j<NVARS; j++) 
      printf(" %d ", population[i].gene[j]);
  }
}

/*********************************************************************/
/*                                                                   */
/*  Initialization function: Initializes the values of genes         */
/*  (binary digits) for a new string.                                */
/*                                                                   */
/*********************************************************************/

void initialize(void)
{
  int i, j;

  /* Init population */ 
  for (i=0; i < POPSIZE; i++) {
     population[i].fitness = 0;
     population[i].rfitness = 0;
     population[i].cfitness = 0;
     for (j=0; j < NVARS; j++) {
        population[i].gene[j] = randbit(); 
     }
  }
}
 

/*********************************************************************/
/*                                                                   */
/*  Random value generator: Generates a random binary digit.         */ 
/*                                                                   */
/*********************************************************************/

int randbit()
{
  return (int) (rand() % 2);
}


 
/*********************************************************************/
/*                                                                   */
/*  Evaluate function: This takes a user defined function.           */
/*  Each time this is changed, the code has to be recompiled.        */
/*  The current function is : a simple 'sum of bits'.                */
/*                                                                   */
/*********************************************************************/
 
void evaluate (void)
{
  int mem;
  int i;
 
  for (mem=0; mem < POPSIZE; mem++) {
     population[mem].fitness = 0;
 
     for (i=0; i < NVARS; i++)
       population[mem].fitness += population[mem].gene[i];
  }
}
 
/*********************************************************************/
/*                                                                   */
/*  Keep_the_best function: This function keeps track of the         */
/*  best member of the population. Note that the last entry in       */
/*  the array Population holds a copy of the best individual         */
/*                                                                   */
/*********************************************************************/
 
void keep_the_best(void)
{
  int i;
  int cur_best;   /* best individual */  

  cur_best = 0;   /* stores the index of the best individual */

  for (i=0; i < POPSIZE; i++) {
     if (population[i].fitness > population[cur_best].fitness) 
        cur_best = i;
  }
 
  if (population[cur_best].fitness > population[POPSIZE].fitness) {
    population[POPSIZE].fitness = population[cur_best].fitness;
    for (i=0; i < NVARS; i++)
      population[POPSIZE].gene[i] = population[cur_best].gene[i];
  } 
}

/*********************************************************************/
/*                                                                   */
/*  Mutation: Random uniform mutation. A variable selected for       */
/*  mutation is replaced by a an inverted binary digit.              */
/*                                                                   */
/*********************************************************************/
 
void mutate(void)
{
  int i, j;
  double lbound, hbound;
  double x;
 
  for (i=0; i < POPSIZE; i++) {
     for (j=0; j < NVARS; j++) {
 
        x = rand() % 1000 / 1000.0;
 
        if (x < PMUTATION) {
 
           /* Perform bit-flipping Mutation */
           if (population[i].gene[j] == 1)
              population[i].gene[j] = 0;
           else
              population[i].gene[j] = 1;
        }
     }
  }
}


       
/*********************************************************************/
/*                                                                   */
/*  Report function: Reports progress of the simulation. Data        */
/*  dumped into the output file are separated by commas              */
/*                                                                   */
/*********************************************************************/

void report(void)
{
  int i;
  double best_val;        /* best population fitness */
  double avg;             /* avg population fitness  */
  double stddev;          /* std. deviation of population fitness */
  double sum_square;      /* sum of square for std. calc */
  double square_sum;      /* square of sum for std. calc */
  double sum;             /* total population fitness  */

  sum = 0.0;
  sum_square = 0.0;

  for (i=0; i < POPSIZE; i++) {
     sum += population[i].fitness;
     sum_square += population[i].fitness * population[i].fitness;
  }

  avg = sum / (double) POPSIZE;
  square_sum = avg * avg * (double) POPSIZE;

  if (POPSIZE == 1) 
    stddev = 0;
  else
    stddev = sqrt((sum_square - square_sum) / (POPSIZE - 1));

  best_val = population[POPSIZE].fitness;

  fprintf(galog, "\n%5d       %6.3f    %6.3f     %6.3f ", generation, 
                                     best_val, avg, stddev);
}

/*********************************************************************/
/*                                                                   */
/*  Main function: Each generation involves selecting the best       */
/*  members, performing crossover and mutation and then              */
/*  evaluating the resulting population, until the terminating       */
/*  condition is satisfied.                                          */
/*                                                                   */
/*********************************************************************/

void main (void)
{
  int i;

  if ((galog = fopen("brandomHill.out", "w")) == NULL) {
     fprintf(stderr, "\nCannot open output file : brandomHill.out \n");
     exit(1);
  }

  fprintf(galog, "\n generation   best      average   standard  \n"); 
  fprintf(galog, "   number      value     fitness   deviation \n"); 

  generation = 0;

  initialize();
  evaluate();
  keep_the_best();
 
  while (generation < MAXGENS) {
     generation++;
     mutate();
     evaluate();
     keep_the_best();
     report();
  }
  
  fprintf(galog, "\n\n  Simulation completed\n");
  fprintf(galog, "\n  Best Member:   \n");

  for (i=0; i < NVARS; i++) {
    fprintf(galog, "\n var(%d) = %d", i, population[POPSIZE].gene[i]);
  }

 
  fprintf(galog, "\n\n Best fitness = %d", population[POPSIZE].fitness);
  fclose(galog);

  printf("Success!\n");
}


/****************************************************************************/


