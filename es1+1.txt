/*********************************************************************/
/*                                                                   */
/*  FILENAME : ~/CExamples/GAs/es1+1.c                               */
/*                                                                   */
/*  AUTHOR : K Williams                                              */
/*                                                                   */
/*  LAST UPDATE : 3 / Mar / 1997                                     */
/*                                                                   */
/*  COMMENTS :                                                       */
/*                                                                   */
/*  This is an implementation of the original two-membered           */
/*  evolutionary strategy (ES(1+1)) program of Schwefel & Rechenberg */
/*  which uses floating point-vectors, Gaussian noise mutation,      */
/*  no crossover, and a 1+1 population algorithm.                    */
/*                                                                   */
/*  COMPILER : gcc -O -o es1+1  es1+1.c -lm                          */
/*                                                                   */
/*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* Change any of these parameters to match your needs */

#define POPSIZE 1       /* population size will be */
#define MAXGENS 100      /* max. number of generations */
#define NVARS 50         /* no. of problem variables */
#define PMUTATION 0.25  /* probability of mutation - 25 in every 100 */
#define TRUE 1
#define FALSE 0

int generation;                  /* current generation no. */
int cur_best;                    /* best individual */  
FILE *galog;                     /* an output file */

struct genotype  /* genotype (GT), a member of the population */
{
	double gene[NVARS];      /* a string of variables */
	double upper[NVARS];     /* GT`s variables upper bound */
	double lower[NVARS];     /* GT`s variables lower bound */
	double fitness;          /* GT`s fitness */
	double rfitness;         /* relative fitness */
	double cfitness;         /* cumulative fitness */
};

struct genotype population[POPSIZE+1];        /* population */
struct genotype newpopulation[POPSIZE+1];     /* new population */
                                              /* replaces the old population */

/* Prototypes of functions used by this genetic algorithm */

void initialize(void);
double randval(double, double);
void evaluate(void);
void mutate(void);
double delta2 (int t, int y);
void report(void);
void printPop(int g);


/*********************************************************************/
/*                                                                   */
/*  Initialization function: Initializes the values of genes         */
/*  within the variables bounds. It also initializes (randomly)      */
/*  all gene values for the 1 (POPSIZE) member of the population. It */
/*  sets upper and lower bounds of each variable. It randomly        */
/*  generates values between these bounds for each gene of the       */
/*  genotype in the population.                                      */
/*                                                                   */
/*********************************************************************/

void initialize(void)
{
  int i, j;
  double l, u;
  double lbound[NVARS], ubound[NVARS];
  
  /* Initialise variable bounds */
  for (i=0; i < NVARS; i++) {
     lbound[i] = 0.0;
     ubound[i] = 10.0; 
  }
  
  /* Initialise variables within the bounds */
  j=0;
  for (i=0; i < NVARS; i++) {
    population[j].lower[i] = lbound[i];
    population[j].upper[i] = ubound[i];
  }


  /* For the one member of the 'population' */
  j = POPSIZE;
  population[j].fitness = 0;
  population[j].rfitness = 0;
  population[j].cfitness = 0;

  /* Initialise variables within the bounds */
  for (i=0; i < NVARS; i++) {
     l = lbound[i];
     u = ubound[i];

     population[j].lower[i] = l;
     population[j].upper[i] = u;
     population[j].gene[i] = randval(population[j].lower[i], 
                                     population[j].upper[i]); 
  }
}
 

/*********************************************************************/
/*                                                                   */
/*  Random value generator: Generates a value within bounds          */ 
/*                                                                   */
/*********************************************************************/

double randval(double low, double high)
{
  double val;
  val = ((double) (rand() % 1000) / 1000.0) * (high - low) + low ; 
  return val;
}


/*********************************************************************/
/*                                                                   */
/*  Evaluate function-2: This takes a user defined function.         */
/*  Each time this is changed, the code has to be recompiled.        */
/*  The current function is :  x[1]^2-x[1]*x[2]+x[3]                 */ 
/*                                                                   */
/*********************************************************************/

void evaluate2 (void)
{
  /* NVARS must be == 3 */
  int mem;
  int i;
  double x[NVARS+1];

  for (mem=0; mem <= POPSIZE; mem++) {
     for (i=0; i < NVARS; i++) 
        x[i+1] = population[mem].gene[i];

     population[mem].fitness = (x[1]*x[1]) - (x[1]*x[2]) + x[3];
  }
}

void evaluate (void)     /* simple sum-of-gene (reals) */
{
  int i, mem;

  for (mem=0; mem <= POPSIZE; mem++) {
     population[mem].fitness = 0;

     for (i=0; i < NVARS; i++) 
        population[mem].fitness += population[mem].gene[i];

  }
}

/*********************************************************************/
/*                                                                   */
/*  Mutation: Random uniform mutation. A variable selected for       */
/*  mutation is replaced by a random value between lower and         */
/*  upper bounds of this variable. Only apply mutation to offspring  */
/*  (which is held in the first '0' location of the population       */
/*  array.                                                           */ 
/*                                                                   */
/*********************************************************************/


void mutate (void)
{
  int i, j, k;
  double lower, upper, value;
  double r, x;

  i = 0 ;     /* offspring held in 0 location of array */
  for (j=0; j < NVARS; j++) {

    x = rand() % 1000 / 1000.0;

    if (x < PMUTATION) {

        /* Find the bounds on the variable to be mutated */
        lower = population[i].lower[j];
        upper = population[i].upper[j];

        /* Determine the initial degree of the mutation */
        value = population[i].gene[j];   /* randval(lower, upper); */

        /* Randomly determine whether to add or subtract */
        r = (randval(0,1) >= 0.5) ? 1 : 0;

        if (r == 0) 
           population[i].gene[j] += delta2(generation, upper - value);
        else
        if (r == 1) 
           population[i].gene[j] -= delta2(generation, value - lower);
     }
  }
}


double delta2 (int t, int y)
{
  static int B = 2;   /* degree of non-uniform mutation */
  return (y * (1-pow(randval(0,1), pow(1-t/MAXGENS, (int) B))));	
}


void mutate_old(void)
{
  int i, j;
  double lbound, hbound;
  double x;

  i= 0 ;     /* offspring held in 0 location of array */
  for (j=0; j < NVARS; j++) {

     x = rand() % 1000 / 1000.0;

     if (x < PMUTATION) {
        /* Find the bounds on the variable to be mutated */
 
        lbound = population[i].lower[j];
        hbound = population[i].upper[j];
        population[i].gene[j] = randval(lbound, hbound);
     }
  }
}

 

/*********************************************************************/
/*                                                                   */
/*  Report function: Reports progress of the simulation. Data        */
/*  dumped into the output file are separated by commas              */
/*                                                                   */
/*********************************************************************/

void report(void)
{
  int i;
  double best_val;        /* best population fitness */
  double avg;             /* avg population fitness  */
  double sum_square;      /* sum of square for std. calc */
  double square_sum;      /* square of sum for std. calc */
  double sum;             /* total population fitness  */

  sum = 0.0;
  sum_square = 0.0;

  for (i=0; i < POPSIZE; i++) {
     sum += population[i].fitness;
     sum_square += population[i].fitness * population[i].fitness;
  }

  avg = sum / (double) POPSIZE;
  best_val = population[POPSIZE].fitness;

  fprintf(galog, "\n%5d       %6.3f    %6.3f\n", generation, best_val, avg);

  fprintf(galog, "population[0]       = ");
/*  for (i=0; i<NVARS; i++) 
    fprintf(galog, " %6.3f ", population[0].gene[i]);
*/
  fprintf(galog, " fitness %6.3f \n", population[0].fitness);

  fprintf(galog, "population[POPSIZE] = ");
/*  for (i=0; i<NVARS; i++) 
    fprintf(galog, " %6.3f ", population[POPSIZE].gene[i]);
*/
  
  fprintf(galog, " fitness %6.3f \n", population[POPSIZE].fitness);

}


void printPop(int g)
{
  int i;

  printf("GEN %d population[0] = ", g);

  for (i=0; i<NVARS; i++) {
    printf(" %6.3f ", population[0].gene[i]);
  }
  printf(" fitness %6.3f \n", population[0].fitness);

  for (i=0; i<NVARS; i++) {
    printf(" %6.3f ", population[POPSIZE].gene[i]);
  }
  printf(" fitness %6.3f \n", population[POPSIZE].fitness);

}


/*********************************************************************/
/*                                                                   */
/*  Main function: Each generation involves selecting the best       */
/*  members, performing mutation and then                            */
/*  evaluating the resulting population, until the terminating       */
/*  condition is satisfied.                                          */
/*                                                                   */
/*********************************************************************/


void main (void)
{
  int i, j;

  if ((galog = fopen("es1+1.out", "w")) == NULL) {
     fprintf(stderr, "\nCannot open output file : es1+1.out \n");
     exit(1);
  }

  generation = 0;

  
  fprintf(galog, "\n generation   best      average   standard  \n"); 
  fprintf(galog, "   number      value     fitness   deviation \n"); 

  initialize();

  while (generation < MAXGENS) {

     generation++;

     /* Generate offspring into '0' location of population array */
     /* Copy parent genes with each iteration - i.e. NOT hill-climbing */
     for (j=0; j < NVARS; j++) 
        population[0].gene[j] = population[POPSIZE].gene[j];
     population[0].fitness = population[POPSIZE].fitness;

     /* Mutate offspring to distinguish from parent */
     mutate();  

     evaluate();

/* printPop(generation); */

     report();

/*      if (population[0].fitness > population[POPSIZE].fitness) {
*/
        /* Blindly copy offspring to replace parent es1+1 strategy */
        for (j=0; j < NVARS; j++) 
           population[POPSIZE].gene[j] = population[0].gene[j];
           population[POPSIZE].fitness = population[0].fitness;
/*      } */
  }

 
  fprintf(galog, "\n\n  Simulation completed\n");
  fprintf(galog, "\n  Best Member:   \n");

  for (i=0; i < NVARS; i++) {
    fprintf(galog, "\n var(%d) = %3.3f", i, population[POPSIZE].gene[i]);
  }

 
  fprintf(galog, "\n\n Best fitness = %3.3f", population[POPSIZE].fitness);
  fclose(galog);

  printf("Success!\n");
}


/****************************************************************************/


