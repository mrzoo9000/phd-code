# Contents: phd-code repository#

This repository contains some of code developed for Ph.D for "Evolutionary Algorithms for Automatic Parallelizaton" by Ken Williams (1998).

### Descriptions ###

Most of these examples were created to be 'stand-alone' programs. As such most of them are 
simple example programs which were developed to implement or test and idea. They are stored 
under the **Source** directory and can easily be compiled and run.   

* **ReceivingServlet.html** - Example Java Servlet handing HTTP requests. 
* **ReceivingServlet.java** - Example Java Servlet handing HTTP requests. 
* **anneal.c** - Basic simulated annealing optimization algorithm.
* **anneal2.c** - Basic simulated annealing optimization algorithm with different fitness function. 
* **brandomhill.c** - Random iterative hill-climbing program. 
* **cobra.c** - An example of a cost-based operator-rate adaptation (COBRA) genetic algorithm. 
* **es1+1.c** - An implementation of the original two-membered evolution strategy ES(1+1) parent-replacement strategy of Schwefel and Rechenberg.
* **es1comma1.c** - An implementation of the original two-membered evolution strategy ES(1,1) parent-replacement strategy of Schwefel and Rechenberg. 
* **esm+1.c** - Simple Evolution Strategy using the ES(m+1) replacement strategy.
* **esm+l.c** - Simple Evolution Strategy using the ES(m+l) replacement strategy.
* **esmcommal.c** - Simple Evolution Strategy using the ES(m,1) replacement strategy.
* **frandomhill.c** - Floating-point version of iterative hill-climbing program.
* **ga1.c** - Implementation of Simple Genetic Algorithm (SGA).  
* **ga2.c** - Implementation of Simple Genetic Algorithm with different operators. 
* **ga3.c** - Implementation of Simple Genetic Algorithm with variable operator probabilities. 
* **gabinary.c** - Implementation of binary-valued SGA.
* **gavacl.c** - Implementation of Simple Genetic Algorithm with Variable Chromosome Length (GAVaCL). 
* **graycode.c** -  Implementation of binary-valued SGA using Gray-coding.  
* **httpMethodForm.html** - HTML form to generate various HTTP requests. 
* **irandomhill.c** - Integer version of iterative hill-climbing program. 
* **multistart.c** - Multi-start, steepest-ascent iterative hill-climbing program. 
* **mutationgene.c** - SGA with 'mutation-probability' gene attached.  
* **out1.txt** - Example output file. 
* **rubik.txt** - Implementation of program (writen in Prolog) using depth-first search to solve a Rubik's cube. 

END.